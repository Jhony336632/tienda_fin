<?php
include "Admin/fpdf/fpdf.php";
include "Admin/BDD/Conexion.php";
$con="SELECT * FROM clientes c INNER JOIN facturas f ON c.id = f.idcliente INNER JOIN detalles d ON f.idf = d.idFactura INNER JOIN productos p ON p.idp = d.idProducto WHERE f.idf;";
$res = mysqli_query($conn,$con);
;
/*
$filas = array();

while ($fila=mysqli_fetch_array($res, MYSQLI_ASSOC)) {
        $filas['nombre'] = $fila; // Añade el array $fila al final de $filas
        $filas['apellido'] = $fila;
        $filas['detallecl'] = $fila;
        $filas['fnacimiento'] = $fila;
        $filas['idf']= $fila;
        $filas['fecha'] = $fila;
        $filas['idp'] = $fila;
        $filas['Cantidad'] = $fila;
        $filas['nombrepr'] = $fila;
        $filas['IVA'] = $fila;
        $filas['Importe'] = $fila;
        $filas['PrecioVenta'] = $fila;
        $filas['subtotal'] = $fila;
        $filas['total'] = $fila;
}*/

while($row=$res->fetch_assoc()){
    $nombre = $row['nombre'];
    $apellido = $row['apellido'];
    $cedula = $row['detallecl'];
    $fna = $row['fnacimiento'];
    $idf = $row['idf'];
    $fech = $row['fecha'];
    $idp = $row['idp'];
    $can = $row['Cantidad'];
    $nomp = $row['nombrepr'];
    $iva = $row['IVA'];
    $impo = $row['Importe'];
    $prec = $row['PrecioVenta'];
    $sbt= $row['subtotal'];
    $pfin = $row['total'];
}

$pdf = new FPDF($orientation='P',$unit='mm');
$pdf->AddPage();
$pdf->SetFont('Arial','B',20);    
$textypos = 5;
$pdf->setY(19);
$pdf->setX(10);
// Agregamos los datos de la empresa
$pdf->Cell(5,$textypos,"TIENDA DE MASCOTAS");
$pdf->SetFont('Arial','B',10);    
$pdf->setY(30);$pdf->setX(10);
$pdf->Cell(5,$textypos,"DE:");
$pdf->SetFont('Arial','',10);    
$pdf->setY(35);$pdf->setX(10);
$pdf->Cell(5,$textypos,"TIENDA DE MASCOTAS");
$pdf->setY(40);$pdf->setX(10);
$pdf->Cell(5,$textypos,"AV. MALDONADO");
$pdf->setY(45);$pdf->setX(10);
$pdf->Cell(5,$textypos,"02-3254-218");
$pdf->setY(50);$pdf->setX(10);
$pdf->Cell(5,$textypos,"tienda@demascotas.com");

// Agregamos los datos del cliente
$pdf->SetFont('Arial','B',10);    
$pdf->setY(30);$pdf->setX(75);
$pdf->Cell(5,$textypos,"PARA:");
$pdf->SetFont('Arial','',10);    
$pdf->setY(35);$pdf->setX(75);
$pdf->Cell(5,$textypos,'Nombre:'.$nombre." ".$apellido);
$pdf->setY(40);$pdf->setX(75);
$pdf->Cell(5,$textypos,'Cedula:'.$cedula);
$pdf->setY(45);$pdf->setX(75);
$pdf->Cell(5,$textypos,"F.Nacimiento:".$fna);


// Agregamos los datos del cliente
$pdf->SetFont('Arial','B',10);    
$pdf->setY(30);$pdf->setX(135);
$pdf->Cell(5,$textypos,"FACTURA #".$idf);
$pdf->SetFont('Arial','',10);    
$pdf->setY(35);$pdf->setX(135);
$pdf->Cell(5,$textypos,"Fecha:".$fech);
$pdf->setY(40);$pdf->setX(135);
$pdf->Cell(5,$textypos,"");
$pdf->setY(45);$pdf->setX(135);
$pdf->Cell(5,$textypos,"");
$pdf->setY(50);$pdf->setX(135);
$pdf->Cell(5,$textypos,"");

/// Apartir de aqui empezamos con la tabla de productos
$pdf->setY(60);$pdf->setX(135);
    $pdf->Ln();
/////////////////////////////
//// Array de Cabecera
$header = array("IDP", "Descripcion","Cant.","Precio","Importe");
//// Arrar de Productos
$products = array(
	array($idp, $nomp,$can,$prec,$impo),
	array($idp, $nomp,$can,$prec,$impo),
	array($idp, $nomp,$can,$prec,$impo),
	array($idp, $nomp,$can,$prec,$impo),
	array($idp, $nomp,$can,$prec,$impo),
	array($idp, $nomp,$can,$prec,$impo),
);
    // Column widths
    $w = array(20, 95, 20, 25, 25);
    // Header
    for($i=0;$i<count($header);$i++)
        $pdf->Cell($w[$i],7,$header[$i],1,0,'C');
    $pdf->Ln();
    // Data
    $total = 0;
    foreach($products as $row)
    {
        $pdf->Cell($w[0],6,$row[0],1);
        $pdf->Cell($w[1],6,$row[1],1);
        $pdf->Cell($w[2],6,number_format($row[2]),'1',0,'R');
        $pdf->Cell($w[3],6,"$ ".number_format($row[3],2,".",","),'1',0,'R');
        $pdf->Cell($w[4],6,"$ ".number_format($row[3]*$row[2],2,".",","),'1',0,'R');

        $pdf->Ln();
        $total+=$row[3]*$row[2];

    }
/////////////////////////////
//// Apartir de aqui esta la tabla con los subtotales y totales
$yposdinamic = 60 + (count($products)*10);

$pdf->setY($yposdinamic);
$pdf->setX(235);
    $pdf->Ln();
/////////////////////////////
$header = array("", "");
$data2 = array(
	array("Subtotal",$sbt),
	array("Iva", $iva),
	array("Total", $pfin),
);
    // Column widths
    $w2 = array(40, 40);
    // Header

    $pdf->Ln();
    // Data
    foreach($data2 as $row)
    {
$pdf->setX(115);
        $pdf->Cell($w2[0],6,$row[0],1);
        $pdf->Cell($w2[1],6,"$ ".number_format($row[1], 2, ".",","),'1',0,'R');

        $pdf->Ln();
    }
/////////////////////////////

$yposdinamic += (count($data2)*10);
$pdf->SetFont('Arial','B',10);    

$pdf->setY($yposdinamic);
$pdf->setX(10);
$pdf->Cell(5,$textypos,"TERMINOS Y CONDICIONES");
$pdf->SetFont('Arial','',10);    

$pdf->setY($yposdinamic+10);
$pdf->setX(10);
$pdf->Cell(5,$textypos,"El usuario debe cancelar.");
$pdf->setY($yposdinamic+20);
$pdf->setX(10);

$pdf->output();
?>
